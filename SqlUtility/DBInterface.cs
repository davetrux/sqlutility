//Copyright (c) 2012 David Truxall
using System;
using System.Data;

namespace SqlUtility
{
    interface DBInterface: IDisposable
    {
        string GetConnectionString();

        string GetConnectionString(string databaseName);

        DataTable ListDatabases();

        DataTable ListDBTables(string databaseName);

        DataTable ListDBStoredProcs(string databaseName);

        T GetConnection<T>();

        bool TestConnection();

        string CreateInsertStatement(string databaseName, string tableName);

        string CreateSelectStatement(string databaseName, string tableName);

        string CreateCSParameterList(string databaseName, string storedProcName);

        string ReverseEngineerData(string databaseName, string tableName, bool includeKey);



    }
}
