//Copyright (c) 2012 David Truxall
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

namespace SqlUtility
{
    class SQLServerDB: DBInterface
    {
        private string connectionString;

        private SqlConnection connection;
        private bool disposed = false;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="userId">SQL Server user ID</param>
        /// <param name="password">Sql Server password</param>
        /// <param name="serverName">Name of server\instance</param>
        /// <remarks>The constructor creates the connection string</remarks>
        public SQLServerDB(string userId, string password, string serverName)
        {
            try
            {
			    StringBuilder connString = new StringBuilder();
                //string fileContents;
                //using (System.IO.StreamReader sr = new System.IO.StreamReader(@"C:\Test.txt"))
                //{
                //    fileContents = sr.ReadToEnd();
                //}

                connString.Append("server=");
                connString.Append(serverName);
                connString.Append(";user id=");
                connString.Append(userId);
                connString.Append(";pwd=");
                connString.Append(password);
                connString.Append(";");

                this.connectionString = connString.ToString();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="serverName">Name of server\instance</param>
        /// <remarks>The constructor creates the connection string</remarks>
        public SQLServerDB(string serverName)
        {
            try
            {
                StringBuilder connString = new StringBuilder();

                connString.Append("server=");
                connString.Append(serverName);
                connString.Append(";integrated Security=true;");

                this.connectionString = connString.ToString();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void Dispose()
        {
            try
            {
                if (!this.disposed)
                {
                    if (this.connection != null)
                    {
                        if (this.connection.State == ConnectionState.Open)
                        {
                            this.connection.Close();
                        }
                        this.connection.Dispose();
                        this.disposed = true;
                    }
                }
            }
            catch (Exception ex)
            {
                
            }
        }

        public static List<string> GetServerNames()
        {
            var result = new List<string>();
            using (DataTable table = System.Data.Sql.SqlDataSourceEnumerator.Instance.GetDataSources())
            {
                foreach (DataRow server in table.Rows)
                {
                    result.Add(server[table.Columns["ServerName"]].ToString());
                }
            }
            
            return result;
        }

        #region "Connection/Data Methods"

        /// <summary>
        /// Return an open connection to the database
        /// </summary>
        /// <returns>An open SqlConnection or null</returns>
        private SqlConnection GetOpenConnection()
        {
            try
            {
                if (this.connection == null)
                {
                    this.connection = new SqlConnection(this.connectionString);
                    this.connection.Open();
                }

                if (this.connection.State != ConnectionState.Open)
                {
                    this.connection.Open();
                }

                return this.connection;

            }
            catch (Exception ex)
            {

                return null;
            }
        }


        /// <summary>
        /// Test the connection to the database
        /// </summary>
        /// <returns></returns>
        public bool TestConnection()
        {
            try
            {
                SqlConnection conn = this.GetOpenConnection();
                if (conn == null)
                {
                    return false;
                }
                else
                {
                    if (conn.State == ConnectionState.Open)
                    {
                        conn.Close();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        /// <summary>
        /// Returns a connection string without specifiying a database
        /// </summary>
        /// <returns></returns>
        public string GetConnectionString()
        {
            return this.connectionString;
        }

        /// <summary>
        /// Returns a connection string with a specific database
        /// </summary>
        /// <param name="databaseName">The database name to add to the connection string</param>
        /// <returns></returns>
        public string GetConnectionString(string databaseName)
        {
            if (databaseName != string.Empty)
            {
                return this.connectionString + "initial catalog=" + databaseName + ";";
            }
            else
            {
                return this.GetConnectionString();
            }
        }

        public T GetConnection<T>()
        {
            return default(T);
        }


        private DataTable FillDataTable(string databaseName, string sqlStatement)
        {
            try
            {
                DataTable result = new DataTable();
                SqlConnection conn = this.GetOpenConnection();
                conn.ChangeDatabase(databaseName);
                using (SqlDataAdapter da = new SqlDataAdapter(sqlStatement, conn))
                {
                    da.Fill(result);
                }
                conn.Close();
                return result;
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }

        #endregion


        #region "Database Item List Methods"

        /// <summary>
        /// Gets a list of databases in the current SQL Server based on permissions
        /// </summary>
        /// <returns>A datatable containing server names</returns>
        public DataTable ListDatabases()
        {
            try
            {
                DataTable dt = new DataTable();
                SqlConnection conn = this.GetOpenConnection();
                if (conn != null)
                {
                    string sql = "select [Name] from master.dbo.sysdatabases WHERE IsNull(HAS_DBACCESS ([Name]),0)=1 order by [Name]";
                    using (SqlDataAdapter da = new SqlDataAdapter(sql, conn))
                    {
                        da.Fill(dt);

                        if (dt.Rows.Count > 0)
                        {
                            DataRow dr = dt.NewRow();
                            dr[0] = "";
                            dt.Rows.InsertAt(dr, 0);
                        }
                        conn.Close();
                    }
                }
                return dt;
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }


        private DataTable ListDBItems(string databaseName, string sqlStatement)
        {
            DataTable dt = new DataTable();
            try
            {
                if (databaseName != string.Empty)
                {
                    SqlConnection conn = this.GetOpenConnection();

                    conn.ChangeDatabase(databaseName);

                    using (SqlDataAdapter da = new SqlDataAdapter(sqlStatement, conn))
                    {
                        da.Fill(dt);
                    }
                    conn.Close();
                }
                return dt;
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public DataTable ListDBTables(string databaseName)
        {
            return this.ListDBItems(databaseName, "select [name] from sysobjects where [xtype] = 'U' order by [name]");
        }

        public DataTable ListDBStoredProcs(string databaseName)
        {
            return this.ListDBItems(databaseName, "select [name] from sysobjects where [xtype] = 'P' order by [name]");
        }
        
        #endregion


        #region "Create Sql Statement Methods"

        public string CreateInsertStatement(string databaseName, string tableName)
        {
			StringBuilder results = new StringBuilder();
            try
            {
                string sql = string.Format("select c.name from sysobjects o join syscolumns c on c.id = o.id where o.name = '{0}'", tableName);
                using (DataTable columns = this.FillDataTable(databaseName, sql))
                {
                    if (columns.Rows.Count > 0)
                    {
                        results.AppendFormat("insert into {0} (", tableName);
                        foreach (DataRow item in columns.Rows)
                        {
                            results.Append(item.ItemArray[0].ToString());
                            if (!item.Equals(columns.Rows[columns.Rows.Count-1]))                            
                            {
                                results.Append(", ");
                            }
                        }
                        results.Append(") values (");
                        return results.ToString();
                    }
                    else
                    {
                        return "Table not found";
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public string CreateSelectStatement(string databaseName, string tableName)
        {
            StringBuilder results = new StringBuilder();
            try
            {
                string sql = string.Format("select c.name from sysobjects o join syscolumns c on c.id = o.id where o.name = '{0}'", tableName);

                using (DataTable columns = this.FillDataTable(databaseName, sql))
                {
                    if (columns.Rows.Count > 0)
                    {
                        results.Append("select ");
                        foreach (DataRow item in columns.Rows)
                        {
                            results.Append(item.ItemArray[0].ToString());
                            if (!item.Equals(item.ItemArray[columns.Rows.Count - 1]))
                            {
                                results.Append(", ");
                            }
                        }
                        results.AppendFormat(" from {0}", tableName);

                        return results.ToString();
                    }
                    else
                    {
                       return "Table not found";
                    }
                }
            }
            catch (System.Exception ex)
            {
                return string.Empty;
            }
        }

        public string CreateCSParameterList(string databaseName, string storedProcName)
        {
            return string.Empty;
        }

        public string ReverseEngineerData(string databaseName, string tableName, bool includeKey)
        {
            return string.Empty;
        }


        #endregion
        
    }
}
