//Copyright (c) 2012 David Truxall
using System;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace SqlUtility
{
    /// <summary>
    /// Summary description for Form1.
    /// </summary>
    public class MainForm : System.Windows.Forms.Form
    {

        private string ConnectionString
        {
            get
            {
                //return this.GetConnectionString();
                return this.sqlServer.GetConnectionString();
            }
        }

        private string Cstring;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button TestConnection;
        private System.Windows.Forms.TextBox ServerName;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Results;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button InsertStatement;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button ParameterLister;
        private System.Windows.Forms.ComboBox DatabaseName;
        private System.Windows.Forms.Button Lookup;
        private System.Windows.Forms.ComboBox TableList;
        private System.Windows.Forms.ComboBox StoredProcedures;
        private System.Windows.Forms.GroupBox ActionsGroup;
        private Button SelectStatement;
        private Button UpdateStatement;
        private Button ConnectionStringPrint;
        private RadioButton windowsCred;
        private Label label6;
        private RadioButton sqlCred;
        private TextBox sqlId;
        private Label label7;
        private TextBox sqlPassword;
        private Label label8;

        private SQLServerDB sqlServer;
        private DataTable databaseList;
        private DataTable spList;
        private DataTable tableList;
        private GroupBox groupBox2;
        private CheckBox includeKey;
        private Button serverList;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        public MainForm()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            System.Windows.Forms.Application.EnableVisualStyles();

        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
                if (this.sqlServer != null)
                    this.sqlServer.Dispose();
                if (this.databaseList != null)
                    this.databaseList.Dispose();
                if (this.spList != null)
                    this.spList.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.TestConnection = new System.Windows.Forms.Button();
            this.ServerName = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.sqlPassword = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.sqlId = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.sqlCred = new System.Windows.Forms.RadioButton();
            this.windowsCred = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.ConnectionStringPrint = new System.Windows.Forms.Button();
            this.DatabaseName = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Results = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.InsertStatement = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.ParameterLister = new System.Windows.Forms.Button();
            this.Lookup = new System.Windows.Forms.Button();
            this.TableList = new System.Windows.Forms.ComboBox();
            this.StoredProcedures = new System.Windows.Forms.ComboBox();
            this.ActionsGroup = new System.Windows.Forms.GroupBox();
            this.includeKey = new System.Windows.Forms.CheckBox();
            this.UpdateStatement = new System.Windows.Forms.Button();
            this.SelectStatement = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.serverList = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.ActionsGroup.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(26, 135);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Database Name:";
            // 
            // TestConnection
            // 
            this.TestConnection.Location = new System.Drawing.Point(365, 87);
            this.TestConnection.Name = "TestConnection";
            this.TestConnection.Size = new System.Drawing.Size(208, 24);
            this.TestConnection.TabIndex = 2;
            this.TestConnection.Text = "Test and Save Connection String";
            this.TestConnection.Click += new System.EventHandler(this.TestConnection_Click);
            // 
            // ServerName
            // 
            this.ServerName.Location = new System.Drawing.Point(112, 87);
            this.ServerName.Name = "ServerName";
            this.ServerName.Size = new System.Drawing.Size(232, 20);
            this.ServerName.TabIndex = 4;
            this.ServerName.Text = "L-0980";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.serverList);
            this.groupBox1.Controls.Add(this.sqlPassword);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.ServerName);
            this.groupBox1.Controls.Add(this.sqlId);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.sqlCred);
            this.groupBox1.Controls.Add(this.windowsCred);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.ConnectionStringPrint);
            this.groupBox1.Controls.Add(this.DatabaseName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.TestConnection);
            this.groupBox1.Location = new System.Drawing.Point(12, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(604, 155);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Get Connected";
            // 
            // sqlPassword
            // 
            this.sqlPassword.Enabled = false;
            this.sqlPassword.Location = new System.Drawing.Point(445, 18);
            this.sqlPassword.Name = "sqlPassword";
            this.sqlPassword.Size = new System.Drawing.Size(128, 20);
            this.sqlPassword.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(409, 21);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Pwd:";
            // 
            // sqlId
            // 
            this.sqlId.Enabled = false;
            this.sqlId.Location = new System.Drawing.Point(298, 18);
            this.sqlId.Name = "sqlId";
            this.sqlId.Size = new System.Drawing.Size(95, 20);
            this.sqlId.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(274, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(21, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "ID:";
            // 
            // sqlCred
            // 
            this.sqlCred.AutoSize = true;
            this.sqlCred.Location = new System.Drawing.Point(187, 18);
            this.sqlCred.Name = "sqlCred";
            this.sqlCred.Size = new System.Drawing.Size(74, 17);
            this.sqlCred.TabIndex = 11;
            this.sqlCred.Text = "Sql Server";
            this.sqlCred.CheckedChanged += new System.EventHandler(this.sqlCred_CheckedChanged);
            // 
            // windowsCred
            // 
            this.windowsCred.AutoSize = true;
            this.windowsCred.Checked = true;
            this.windowsCred.Location = new System.Drawing.Point(112, 18);
            this.windowsCred.Name = "windowsCred";
            this.windowsCred.Size = new System.Drawing.Size(69, 17);
            this.windowsCred.TabIndex = 10;
            this.windowsCred.TabStop = true;
            this.windowsCred.Text = "Windows";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Credentials:";
            // 
            // ConnectionStringPrint
            // 
            this.ConnectionStringPrint.Enabled = false;
            this.ConnectionStringPrint.Location = new System.Drawing.Point(365, 118);
            this.ConnectionStringPrint.Name = "ConnectionStringPrint";
            this.ConnectionStringPrint.Size = new System.Drawing.Size(208, 22);
            this.ConnectionStringPrint.TabIndex = 8;
            this.ConnectionStringPrint.Text = "Show Connection String";
            this.ConnectionStringPrint.Click += new System.EventHandler(this.ConnectionStringPrint_Click);
            // 
            // DatabaseName
            // 
            this.DatabaseName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.DatabaseName.Enabled = false;
            this.DatabaseName.FormattingEnabled = true;
            this.DatabaseName.Location = new System.Drawing.Point(112, 119);
            this.DatabaseName.Name = "DatabaseName";
            this.DatabaseName.Size = new System.Drawing.Size(232, 21);
            this.DatabaseName.TabIndex = 7;
            this.DatabaseName.SelectedIndexChanged += new System.EventHandler(this.DatabaseName_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(16, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 23);
            this.label2.TabIndex = 6;
            this.label2.Text = "Server Name:";
            // 
            // Results
            // 
            this.Results.Location = new System.Drawing.Point(12, 365);
            this.Results.Multiline = true;
            this.Results.Name = "Results";
            this.Results.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Results.Size = new System.Drawing.Size(648, 240);
            this.Results.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(12, 346);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 16);
            this.label3.TabIndex = 7;
            this.label3.Text = "Results";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(5, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 16);
            this.label4.TabIndex = 8;
            this.label4.Text = "Table Name:";
            // 
            // InsertStatement
            // 
            this.InsertStatement.Enabled = false;
            this.InsertStatement.Location = new System.Drawing.Point(281, 21);
            this.InsertStatement.Name = "InsertStatement";
            this.InsertStatement.Size = new System.Drawing.Size(75, 24);
            this.InsertStatement.TabIndex = 10;
            this.InsertStatement.Text = "Insert";
            this.InsertStatement.Click += new System.EventHandler(this.InsertStatement_Click);
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(11, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 16);
            this.label5.TabIndex = 11;
            this.label5.Text = "Proc Name:";
            // 
            // ParameterLister
            // 
            this.ParameterLister.Enabled = false;
            this.ParameterLister.Location = new System.Drawing.Point(281, 16);
            this.ParameterLister.Name = "ParameterLister";
            this.ParameterLister.Size = new System.Drawing.Size(150, 24);
            this.ParameterLister.TabIndex = 13;
            this.ParameterLister.Text = "Generate Parameter List";
            this.ParameterLister.Click += new System.EventHandler(this.ParameterLister_Click);
            // 
            // Lookup
            // 
            this.Lookup.Enabled = false;
            this.Lookup.Location = new System.Drawing.Point(281, 51);
            this.Lookup.Name = "Lookup";
            this.Lookup.Size = new System.Drawing.Size(120, 24);
            this.Lookup.TabIndex = 14;
            this.Lookup.Text = "Script All Table Data";
            this.Lookup.Click += new System.EventHandler(this.Lookup_Click);
            // 
            // TableList
            // 
            this.TableList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TableList.Enabled = false;
            this.TableList.FormattingEnabled = true;
            this.TableList.Location = new System.Drawing.Point(77, 23);
            this.TableList.Name = "TableList";
            this.TableList.Size = new System.Drawing.Size(196, 21);
            this.TableList.TabIndex = 15;
            // 
            // StoredProcedures
            // 
            this.StoredProcedures.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.StoredProcedures.Enabled = false;
            this.StoredProcedures.FormattingEnabled = true;
            this.StoredProcedures.Location = new System.Drawing.Point(77, 18);
            this.StoredProcedures.Name = "StoredProcedures";
            this.StoredProcedures.Size = new System.Drawing.Size(196, 21);
            this.StoredProcedures.TabIndex = 16;
            // 
            // ActionsGroup
            // 
            this.ActionsGroup.Controls.Add(this.includeKey);
            this.ActionsGroup.Controls.Add(this.UpdateStatement);
            this.ActionsGroup.Controls.Add(this.TableList);
            this.ActionsGroup.Controls.Add(this.Lookup);
            this.ActionsGroup.Controls.Add(this.SelectStatement);
            this.ActionsGroup.Controls.Add(this.InsertStatement);
            this.ActionsGroup.Controls.Add(this.label4);
            this.ActionsGroup.Location = new System.Drawing.Point(12, 185);
            this.ActionsGroup.Name = "ActionsGroup";
            this.ActionsGroup.Size = new System.Drawing.Size(604, 83);
            this.ActionsGroup.TabIndex = 17;
            this.ActionsGroup.TabStop = false;
            this.ActionsGroup.Text = "Generate SQL";
            // 
            // includeKey
            // 
            this.includeKey.AutoSize = true;
            this.includeKey.Checked = true;
            this.includeKey.CheckState = System.Windows.Forms.CheckState.Checked;
            this.includeKey.Location = new System.Drawing.Point(408, 57);
            this.includeKey.Name = "includeKey";
            this.includeKey.Size = new System.Drawing.Size(107, 17);
            this.includeKey.TabIndex = 16;
            this.includeKey.Text = "Include Key Field";
            this.includeKey.UseVisualStyleBackColor = true;
            // 
            // UpdateStatement
            // 
            this.UpdateStatement.Enabled = false;
            this.UpdateStatement.Location = new System.Drawing.Point(445, 21);
            this.UpdateStatement.Name = "UpdateStatement";
            this.UpdateStatement.Size = new System.Drawing.Size(75, 24);
            this.UpdateStatement.TabIndex = 14;
            this.UpdateStatement.Text = "Update";
            this.UpdateStatement.Click += new System.EventHandler(this.UpdateStatement_Click);
            // 
            // SelectStatement
            // 
            this.SelectStatement.Enabled = false;
            this.SelectStatement.Location = new System.Drawing.Point(363, 21);
            this.SelectStatement.Name = "SelectStatement";
            this.SelectStatement.Size = new System.Drawing.Size(75, 24);
            this.SelectStatement.TabIndex = 11;
            this.SelectStatement.Text = "Select";
            this.SelectStatement.Click += new System.EventHandler(this.SelectStatement_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.StoredProcedures);
            this.groupBox2.Controls.Add(this.ParameterLister);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(12, 280);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(604, 51);
            this.groupBox2.TabIndex = 18;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Generate .Net code";
            // 
            // button1
            // 
            this.serverList.Location = new System.Drawing.Point(252, 50);
            this.serverList.Name = "button1";
            this.serverList.Size = new System.Drawing.Size(121, 23);
            this.serverList.TabIndex = 19;
            this.serverList.Text = "Look for Servers";
            this.serverList.UseVisualStyleBackColor = true;
            this.serverList.Click += new System.EventHandler(this.FindServers_Click);
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(688, 625);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.Results);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ActionsGroup);
            this.Name = "MainForm";
            this.Text = "C# Sql Helper";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ActionsGroup.ResumeLayout(false);
            this.ActionsGroup.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.Run(new MainForm());
        }


        /// <summary>
        /// Tests a connectionstring for the credentials given and database chosen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TestConnection_Click(object sender, System.EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (this.sqlCred.Checked)
                {
                    if (this.sqlId.Text == "")
                    {
                        MessageBox.Show("Please enter a Sql Server Id");
                        return;
                    }
                    if (this.sqlPassword.Text == "")
                    {
                        MessageBox.Show("Please enter a Sql Server Password");
                        return;
                    }

                    this.sqlServer = new SQLServerDB(this.sqlId.Text, this.sqlPassword.Text, this.ServerName.Text);

                }
                else
                {
                    this.sqlServer = new SQLServerDB(this.ServerName.Text);
                }

                if (this.sqlServer.TestConnection())
                {
                    this.Cstring = this.sqlServer.GetConnectionString();
                    this.databaseList = sqlServer.ListDatabases();
                    if (databaseList.Rows.Count > 0)
                    {
                        this.DatabaseName.Enabled = true;
                        this.DatabaseName.DataSource = databaseList;
                        this.DatabaseName.ValueMember = databaseList.Columns[0].ColumnName;
                        this.ConnectionStringPrint.Enabled = true;
                    }

                    this.Results.Text = "Test Connection succeded.";
                }
                else
                {
                    this.Results.Text = "Test Connection failed.";

                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Returns a connection string based on whether or not the database is selected
        /// </summary>
        /// <returns></returns>
        private string GetConnectionString()
        {
            if (this.DatabaseName.SelectedValue.ToString() != "")
            {
                return this.Cstring + "initial catalog=" + this.DatabaseName.SelectedValue.ToString() + ";";
            }
            else
            {
                return this.Cstring;
            }
        }


        /// <summary>
        /// Creates an insert statement stub
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InsertStatement_Click(object sender, System.EventArgs e)
        {
            try
            {
                this.Results.Text = this.sqlServer.CreateInsertStatement(this.DatabaseName.SelectedValue.ToString(),
                    this.TableList.SelectedValue.ToString());
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        /// <summary>
        /// Creates a C# parameter list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ParameterLister_Click(object sender, System.EventArgs e)
        {

            SqlConnection Conn = null;
            SqlCommand Cmd = new SqlCommand();
            StringBuilder output = new StringBuilder();

            try
            {
                Conn = new SqlConnection(this.GetConnectionString());

                Conn.Open();
                Cmd.Connection = Conn;
                Cmd.CommandText = this.StoredProcedures.SelectedValue.ToString();
                Cmd.CommandType = CommandType.StoredProcedure;

                SqlCommandBuilder.DeriveParameters(Cmd);

                int i;
                int counter = 0;

                for (i = 0; i < Cmd.Parameters.Count; i++)
                {
                    if (Cmd.Parameters[i].ParameterName.ToUpper() != "@RETURN_VALUE")
                    {
                        output.Append("ParameterList[");
                        output.Append(counter);
                        output.Append("] = new SqlParameter(\"");
                        output.Append(Cmd.Parameters[i].ParameterName);
                        output.Append("\", SqlDbType.");
                        output.Append(Cmd.Parameters[i].SqlDbType.ToString());
                        if (Cmd.Parameters[i].Size > 0)
                        {
                            output.Append(", ");
                            output.Append(Cmd.Parameters[i].Size);
                        }
                        output.Append(");");
                        if (Cmd.Parameters[i].Direction == ParameterDirection.Output)
                        {
                            output.Append(Environment.NewLine);
                            output.Append("ParameterList[");
                            output.Append(counter);
                            output.Append("].Direction = ParameterDirection.Output");
                        }
                        output.Append(Environment.NewLine);
                        output.Append("ParameterList[");
                        output.Append(counter);
                        output.Append("].Value = ");
                        output.Append(Environment.NewLine);
                        counter++;
                    }
                }

                this.Results.Text = output.ToString();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (Conn != null)
                {
                    if (Conn.State == ConnectionState.Open)
                    {
                        Conn.Close();
                    }
                    Conn.Dispose();
                }
            }
        }


        /// <summary>
        /// Creates SQL Statements to recreate lookup data,
        /// reverse engineering the data in the table
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Lookup_Click(object sender, System.EventArgs e)
        {
            DataTable columns = new DataTable();
            DataTable data = new DataTable();
            SqlDataAdapter da = null;
            StringBuilder results = new StringBuilder();
            StringBuilder values = new StringBuilder();
            StringBuilder query = new StringBuilder();
            try
            {
                string tableName = this.TableList.SelectedValue.ToString();
                string sql = "use " + this.DatabaseName.SelectedValue.ToString() +
                             "; select c.name, c.colstat from sysobjects o join syscolumns c on c.id = o.id where o.name = '" +
                             tableName + "'";
                string cstring = this.ConnectionString;
                da = new SqlDataAdapter(sql, cstring);
                da.Fill(columns);

                if (columns.Rows.Count > 0)
                {
                    results.Append("insert into ");
                    query.Append("use " + this.DatabaseName.SelectedValue.ToString() + "; select ");
                    results.Append(this.TableList.SelectedValue.ToString());
                    results.Append(" (");
                    int i;
                    string colName;
                    for (i = 0; i < columns.Rows.Count; i++)
                    {
                        if (string.Equals(columns.Rows[i].ItemArray[1].ToString(), "1") && !this.includeKey.Checked)
                        {
                            continue;
                        }
                        colName = columns.Rows[i].ItemArray[0].ToString();
                        if (colName.Contains(" "))
                        {
                            colName = "[" + colName + "]";
                        }
                        results.Append(colName);
                        query.Append(colName);
                        if (i < columns.Rows.Count - 1)
                        {
                            results.Append(", ");
                            query.Append(", ");
                        }
                    }
                    results.Append(") values (");
                    query.Append(" from [");
                    query.Append(this.TableList.SelectedValue.ToString().ToLower());
                    query.Append("]");

                    da.Dispose();
                    columns.Dispose();

                    sql = query.ToString();

                    da = new SqlDataAdapter(sql, cstring);
                    da.Fill(data);
                    int j;
                    string dataType;

                    if (data.Rows.Count > 0)
                    {
                        foreach (DataRow r in data.Rows)
                        {
                            values.Append(results.ToString());
                            for (j = 0; j < r.ItemArray.Length; j++)
                            {
                                dataType = r.Table.Columns[j].DataType.ToString().ToLower();
                                if (dataType == "system.string"
                                    || dataType == "system.datetime"
                                    || dataType == "system.guid")
                                {
                                    values.Append("'");
                                    values.Append(r.ItemArray[j]);
                                    values.Append("'");
                                }
                                else if (r.ItemArray[j] == System.DBNull.Value)
                                {
                                    values.Append("null");
                                }
                                else if (dataType == "system.int32")
                                {
                                    values.Append(r.ItemArray[j].ToString());
                                }
                                else if (dataType == "system.boolean")
                                {
                                    if ((bool) r.ItemArray[j] == true)
                                    {
                                        values.Append("1");
                                    }
                                    else
                                    {
                                        values.Append("0");
                                    }
                                }
                                else
                                {
                                    values.Append(r.ItemArray[j]);
                                }
                                if (j < r.ItemArray.Length - 1)
                                {
                                    values.Append(", ");
                                }
                            }
                            values.Append(");");
                            values.Append(Environment.NewLine);
                        }
                        this.Results.Text = values.ToString();
                    }
                }
                else
                {
                    this.Results.Text = "Table not found";
                }

            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                da.Dispose();
                columns.Dispose();
                data.Dispose();
            }

        }


        /// <summary>
        /// Event handler for database selection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DatabaseName_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            try
            {
                if (this.DatabaseName.SelectedIndex <= 0)
                {
                    return;
                }

                this.tableList =
                    this.sqlServer.ListDBTables(((DataRowView) this.DatabaseName.SelectedItem)[0].ToString());

                if (tableList.Rows.Count > 0)
                {
                    this.TableList.Enabled = true;
                    this.TableList.DataSource = tableList;
                    this.TableList.ValueMember = tableList.Columns[0].ColumnName;
                }

                this.StoredProcedures.Enabled = true;

                this.spList =
                    this.sqlServer.ListDBStoredProcs(((DataRowView) this.DatabaseName.SelectedItem)[0].ToString());

                if (spList.Rows.Count > 0)
                {
                    this.StoredProcedures.Enabled = true;
                    this.StoredProcedures.DataSource = spList;
                    this.StoredProcedures.ValueMember = spList.Columns[0].ColumnName;
                }
                this.TableList.Enabled = true;
                this.InsertStatement.Enabled = true;
                this.ParameterLister.Enabled = true;
                this.Lookup.Enabled = true;
                this.UpdateStatement.Enabled = true;
                this.SelectStatement.Enabled = true;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        /// <summary>
        /// Displays the connnection string in a textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConnectionStringPrint_Click(object sender, EventArgs e)
        {
            this.Results.Text = this.sqlServer.GetConnectionString();
        }


        /// <summary>
        /// Generates a select statement for a chosen table
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectStatement_Click(object sender, EventArgs e)
        {
            DataTable columns = new DataTable();
            SqlDataAdapter da = null;
            StringBuilder results = new StringBuilder();
            try
            {
                string sql = "select c.name from sysobjects o join syscolumns c on c.id = o.id where o.name = '" +
                             this.TableList.SelectedValue.ToString() + "'";
                string cstring = this.GetConnectionString();
                da = new SqlDataAdapter(sql, cstring);
                da.Fill(columns);

                if (columns.Rows.Count > 0)
                {
                    results.Append("select ");
                    int i;
                    for (i = 0; i < columns.Rows.Count; i++)
                    {
                        results.Append(columns.Rows[i].ItemArray[0].ToString());
                        if (i < columns.Rows.Count - 1)
                        {
                            results.Append(", ");
                        }
                    }
                    results.Append(" from ");
                    results.Append(this.TableList.SelectedValue.ToString());
                    //results.Append(") values (");
                    this.Results.Text = results.ToString();
                }
                else
                {
                    this.Results.Text = "Table not found";
                }

            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                da.Dispose();
                columns.Dispose();
            }
        }


        /// <summary>
        /// Generates an update statement for the selected table
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateStatement_Click(object sender, EventArgs e)
        {
            DataTable columns = new DataTable();
            SqlDataAdapter da = null;
            StringBuilder results = new StringBuilder();
            try
            {
                string sql = "select c.name from sysobjects o join syscolumns c on c.id = o.id where o.name = '" +
                             this.TableList.SelectedValue.ToString() + "'";
                string cstring = this.ConnectionString;
                da = new SqlDataAdapter(sql, cstring);
                da.Fill(columns);

                if (columns.Rows.Count > 0)
                {
                    results.Append("update ");
                    results.Append(this.TableList.SelectedValue.ToString());
                    results.Append(" set ");
                    results.Append(Environment.NewLine);
                    int i;
                    for (i = 0; i < columns.Rows.Count; i++)
                    {
                        results.Append(columns.Rows[i].ItemArray[0].ToString());
                        if (i < columns.Rows.Count)
                        {
                            results.Append(" = ,");
                            results.Append(Environment.NewLine);
                        }
                    }
                    results.Append("where");
                    this.Results.Text = results.ToString();
                }
                else
                {
                    this.Results.Text = "Table not found";
                }

            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                da.Dispose();
                columns.Dispose();
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Event handler for credential changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sqlCred_CheckedChanged(object sender, EventArgs e)
        {
            if (this.sqlCred.Checked)
            {
                this.sqlId.Enabled = true;
                this.sqlPassword.Enabled = true;
            }
            else
            {
                this.sqlId.Enabled = false;
                this.sqlPassword.Enabled = false;
            }
        }

        private void FindServers_Click(object sender, EventArgs e)
        {
            this.serverList.Enabled = false;
            this.TestConnection.Enabled = false;
            this.ServerName.Enabled = false;

            this.Results.Text = string.Empty;
            // Set cursor as hourglass
            Cursor.Current = Cursors.WaitCursor;

            var textResult = new StringBuilder();

            var servers = SQLServerDB.GetServerNames();

            if (servers.Count > 0)
            {
                textResult.AppendLine("Servers found:");
                textResult.AppendLine();

                foreach (var item in servers)
                {
                    textResult.AppendLine(item);
                }
            }
            else
            {
                textResult.AppendLine();
                textResult.AppendLine("No servers found");
            }

            this.serverList.Enabled = true;
            this.TestConnection.Enabled = true;
            this.ServerName.Enabled = true;

            this.Results.Text = textResult.ToString();

            Cursor.Current = Cursors.Default;
        }
    }
}
